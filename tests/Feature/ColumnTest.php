<?php

namespace Tests\Feature;

use App\Models\Column;
use Auth;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use phpDocumentor\Reflection\Types\Void_;
use Tests\TestCase;

class ColumnTest extends TestCase
{
    use RefreshDatabase;

    protected $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = \App\Models\User::factory()->make();
    }

    /** @test  */
    public function user_can_be_added_column()
    {
        $this->withoutExceptionHandling();

        //----------------
        // User ID Not Accessabel Error
        //dd($this->user->api_token);
//        $user = \App\Models\User::factory()->make();
//        $user = \App\Models\User::where('api_token', $this->user->api_token)->first();
//        $id = Auth::id();
//        dd($id);
        //------------------

        //$data =  array_merge($this->data(), ['user_id' => $user->id, 'api_token' => $user->api_token]);
        $data =  array_merge($this->data(), ['user_id' => $this->user->id, 'api_token' => $this->user->api_token]);
        $this->post('/api/Columns', $data);
        //dd($data);


        $column = Column::first();

        //dd($column);

        //$this->assertCount(1, Column::all());

        $this->assertEquals('Test Name', $column->Title);
        $this->assertEquals('1', $column->DataType);
        $this->assertEquals(null, $column->FormElement);
        $this->assertEquals(null, $column->ElementWidth);
    }






    private function data()
    {
        return
            [
                //'user_id' => $this->user->id,
                'Title' => 'Test Name',
                'DataType' => 1,
                'FormElement' => null,
                'ElementWidth' => null,
                //'api_token' => $this->user->api_token,
            ];
    }
}
