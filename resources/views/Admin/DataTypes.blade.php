@extends('Admin.master')
@section('content')


<div class="container-fluid">
    <div class="row">
        @include('Admin.section.menu')

        <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">


            <h2>DataTypes</h2>



            <div class="table-responsive">

                <table class="table table-striped table-sm">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>Name</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach ($DataTypes as $dataType)
                        <tr>
                            <td>{{ ($dataType->id) }}</td>
                            <td>{{ ($dataType->Name) }}</td>
                        </tr>
                    @endforeach
                </table>

            </div>
        </main>
    </div>

{{--        <div class="paginate">{!! $DataTypes->links() !!}</div>--}}

</div>







@endsection
