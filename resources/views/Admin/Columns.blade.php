@extends('Admin.master')
@section('content')


<div class="container-fluid">
    <div class="row">
        @include('Admin.section.menu')

        <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">


            <h2>Columns</h2>



            <div class="table-responsive">

                <table class="table table-striped table-sm">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>user_id</th>
                        <th>Title</th>
                        <th>DataType</th>
                        <th>FormElement</th>
                        <th>ElementWidth</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach ($Columns as $column)
                        <tr>
                            <td>{{ ($column->id) }}</td>

                            <td>{{ ($column->user_id) }}</td>
                            <td>{{ ($column->Title) }}</td>
                            <td>{{ ($column->DataType) }}</td>
                            <td>{{ ($column->FormElement) }}</td>
                            <td>{{ ($column->ElementWidth) }}</td>

                        </tr>
                    @endforeach
                </table>

            </div>
        </main>
    </div>

{{--        <div class="paginate">{!! $Columns->links() !!}</div>--}}

</div>







@endsection
