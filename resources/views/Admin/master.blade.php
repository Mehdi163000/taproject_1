<!doctype html>
<html lang="en">

@include('Admin.section.head')

<body>

@include('Admin.section.header')

@yield('content')

@include('Admin.section.footer')

</body>
</html>
