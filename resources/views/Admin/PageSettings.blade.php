@extends('Admin.master')
@section('content')


<div class="container-fluid">
    <div class="row">
        @include('Admin.section.menu')

        <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">


            <h2>PageSettings</h2>



            <div class="table-responsive">

                <table class="table table-striped table-sm">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>user_id</th>
                        <th>PageName</th>
                        <th>DataInPage</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach ($PageSettings as $PageSetting)
                        <tr>
                            <td>{{ ($PageSetting->id) }}</td>
                            <td>{{ ($PageSetting->user_id) }}</td>
                            <td>{{ ($PageSetting->PageName) }}</td>
                            <td>{{ ($PageSetting->DataInPage) }}</td>
                        </tr>
                    @endforeach
                </table>

            </div>
        </main>
    </div>

{{--        <div class="paginate">{!! $PageSettings->links() !!}</div>--}}

</div>







@endsection
