<nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
    <div class="position-sticky pt-3">
        <ul class="nav flex-column">
{{--            <li class="nav-item">--}}
{{--                <a class="nav-link active" aria-current="page" href="/Admin/Dashboard">--}}
{{--                    <span data-feather="home"></span>--}}
{{--                    Dashboard--}}
{{--                </a>--}}
{{--            </li>--}}

{{--            <a class="nav-link active" aria-current="page" href="#">--}}
{{--                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home" aria-hidden="true"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>--}}
{{--                Dashboard--}}
{{--            </a>--}}

            <style>
                .ActiveItem {
                    font-weight: bolder;
                    color: yellowgreen;
                }
            </style>

            <li class="nav-item">
                <a class="nav-link @if($Active == 'Users') ActiveItem @endif" href="/Admin/Users">
                    Users
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if($Active == 'Columns') ActiveItem @endif" href="/Admin/Columns">
                    Columns
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if($Active == 'DataTypes') ActiveItem @endif" href="/Admin/DataTypes">
                    DataTypes
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if($Active == 'FormElements') ActiveItem @endif" href="/Admin/FormElements">
                    FormElements
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if($Active == 'ElementWidths') ActiveItem @endif" href="/Admin/ElementWidths">
                    ElementWidths
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if($Active == 'ElementTypes') ActiveItem @endif" href="/Admin/ElementTypes">
                    ElementTypes
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if($Active == 'PageSettings') ActiveItem @endif" href="/Admin/PageSettings">
                    PageSettings
                </a>
            </li>

        </ul>


        {{--        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">--}}
        {{--            <span>Saved reports</span>--}}
        {{--            <a class="link-secondary" href="#" aria-label="Add a new report">--}}
        {{--                <span data-feather="plus-circle"></span>--}}
        {{--            </a>--}}
        {{--        </h6>--}}
        {{--        <ul class="nav flex-column mb-2">--}}
        {{--            <li class="nav-item">--}}
        {{--                <a class="nav-link" href="#">--}}
        {{--                    <span data-feather="file-text"></span>--}}
        {{--                    Current month--}}
        {{--                </a>--}}
        {{--            </li>--}}
        {{--           --}}
        {{--        </ul>--}}
    </div>
</nav>


