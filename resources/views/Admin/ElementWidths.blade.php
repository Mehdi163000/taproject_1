@extends('Admin.master')
@section('content')


<div class="container-fluid">
    <div class="row">
        @include('Admin.section.menu')

        <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">


            <h2>DataTypes</h2>

            <div class="table-responsive">

                <table class="table table-striped table-sm">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>ElementType</th>
                        <th>ElementID</th>
                        <th>Width</th>
                        <th>FontSize</th>
                        <th>FontWeight</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach ($ElementWidths as $ElementWidth)
                        <tr>
                            <td>{{ ($ElementWidth->id) }}</td>
                            <td>{{ ($ElementWidth->ElementType) }}</td>
                            <td>{{ ($ElementWidth->ElementID) }}</td>
                            <td>{{ ($ElementWidth->Width) }}</td>
                            <td>{{ ($ElementWidth->FontSize) }}</td>
                            <td>{{ ($ElementWidth->FontWeight) }}</td>
                        </tr>
                    @endforeach
                </table>

            </div>
        </main>
    </div>

{{--        <div class="paginate">{!! $ElementWidths->links() !!}</div>--}}

</div>







@endsection
