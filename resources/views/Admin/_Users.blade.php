<table class="table table-striped table-sm">
    <thead>
    <tr>
        <th>id</th>
        <th>name</th>
        <th>email</th>
        <th>email_verified_at</th>
        <th>level</th>
        <th>api_token</th>
    </tr>
    </thead>
    <tbody>

    @foreach ($Users as $user)
        <tr>
            <td>{{ ($user->id) }}</td>

            <td>{{ ($user->name) }}</td>
            <td>{{ ($user->email) }}</td>
            <td>{{ ($user->email_verified_at) }}</td>
            <td>{{ ($user->level) }}</td>
            <td>{{ ($user->api_token) }}</td>

        </tr>
    @endforeach
</table>
