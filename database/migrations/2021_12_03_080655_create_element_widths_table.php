<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateElementWidthsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('element_widths', function (Blueprint $table) {
            $table->increments('id');
            //$table->string('ElementType', 30)->nullable();
            $table->smallInteger('ElementType')->unsigned();
            $table->integer('ElementID')->unsigned();
            $table->integer('Width')->nullable();
            $table->integer('FontSize')->nullable();
            $table->string('FontWeight', 30)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('element_widths');
    }
}
