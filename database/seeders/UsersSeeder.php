<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Illuminate\Support\Str;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //----------------------------------------------
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('users')->truncate(); //delete Old Data in Table

        $users = array(
            array(
                'name' => 'Mehdi',
                'email' => 'm@n.com',
                'email_verified_at' => now(),
                'password' => bcrypt('123123'),
                'level' => 'Admin',
                'remember_token' => Str::random(10),



                'api_token' => Str::random(32),
            ),
        );

        DB::table('users')->insert($users);
        //---------------------------------------
        \App\Models\User::factory(10)->create();

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
