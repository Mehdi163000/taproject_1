<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class FormElementsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //----------------------------------------------
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('form_elements')->truncate(); //delete Old Data in Table

        $form_elements = array(
            array('id' => '1','Name' => 'input'),
            array('id' => '2','Name' => 'radiobutton'),
            array('id' => '3','Name' => 'dropdown'),
            array('id' => '4','Name' => 'phone'),
            array('id' => '5','Name' => 'email')
        );

        DB::table('form_elements')->insert($form_elements);
        //---------------------------------------

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
