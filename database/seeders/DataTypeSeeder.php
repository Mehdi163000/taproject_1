<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class DataTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //----------------------------------------------
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('data_types')->truncate(); //delete Old Data in Table

        $data_types = array(
            array('id' => '1','Name' => 'string'),
            array('id' => '2','Name' => 'integer'),
            array('id' => '3','Name' => 'decimal'),
            array('id' => '4','Name' => 'datetime'),
            array('id' => '5','Name' => 'range')
        );

        DB::table('data_types')->insert($data_types);
        //---------------------------------------

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    }
}
