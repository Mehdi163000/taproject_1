<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class ElementTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //----------------------------------------------
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('element_types')->truncate(); //delete Old Data in Table

        $element_types = array(
            array('id' => '1','Name' => 'column'),
            array('id' => '2','Name' => 'input'),
            array('id' => '3','Name' => 'button')
        );

        DB::table('element_types')->insert($element_types);
        //---------------------------------------

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
