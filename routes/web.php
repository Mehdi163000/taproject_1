<?php

use Illuminate\Support\Facades\Route;
//---------------------------------

//Auth::routes();
// Login Routes Detail for Clean Code
Route::get('login', [App\Http\Controllers\Auth\LoginController::class, 'showLoginForm'])->name('login');
Route::post('login', [App\Http\Controllers\Auth\LoginController::class, 'login']);
Route::post('logout', [App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');
Route::get('logout', [App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');
Route::get('register', [App\Http\Controllers\Auth\RegisterController::class, 'showRegistrationForm'])->name('register');
Route::post('register', [App\Http\Controllers\Auth\RegisterController::class, 'register']);

Route::middleware('auth:web')->group(function () {
    Route::get('/', function () {
//        if (auth()->user()->level == 'Admin'){
//            return redirect('/Admin');
//        }

        return view('welcome');

    });

    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
});





