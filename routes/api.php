<?php

use App\Http\Controllers\Api\v1\ColumnApiController;
use App\Http\Controllers\Api\v1\PageSettingApiController;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\v1\ApiAuthController;

Route::post('/Columns', [\App\Http\Controllers\ColumnController::class, 'store']);

Route::post('login', [ApiAuthController::class, 'authenticate']);
Route::post('register', [ApiAuthController::class, 'register']);

Route::group(['middleware' => ['jwt.verify']], function() {
    Route::get('logout', [ApiAuthController::class, 'logout']);
    Route::get('get_user', [ApiAuthController::class, 'get_user']);
    //-------------------------------------------------------------
    Route::get('Columns', [ColumnApiController::class, 'index']);
    Route::post('column/SaveData', [ColumnApiController::class, 'SaveData']);

    Route::post('pageSetting/SaveData', [PageSettingApiController::class, 'SaveData']);
});

