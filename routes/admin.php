<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AdminController;


Route::group(['middleware' => 'IsAdmin'], function () {
    Route::get('/', [AdminController::class, 'index'])->name('admin.index');
    Route::get('/Dashboard', [AdminController::class, 'Dashboard'])->name('admin.Dashboard');
    Route::get('/Users', [AdminController::class, 'Users'])->name('admin.Users');
    Route::get('/Columns', [AdminController::class, 'Columns'])->name('admin.Columns');
    Route::get('/DataTypes', [AdminController::class, 'DataTypes'])->name('admin.DataTypes');
    Route::get('/FormElements', [AdminController::class, 'FormElements'])->name('admin.FormElements');
    Route::get('/ElementWidths', [AdminController::class, 'ElementWidths'])->name('admin.ElementWidths');
    Route::get('/ElementTypes', [AdminController::class, 'ElementTypes'])->name('admin.ElementTypes');
    Route::get('/PageSettings', [AdminController::class, 'PageSettings'])->name('admin.PageSettings');
});
