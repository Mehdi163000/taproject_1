@echo off

::set PATH=%PATH%;C:\Program Files\Git\cmd

:: Laravel
DOSKEY serve=php artisan serve --host=127.0.0.1
::DOSKEY art serve=php artisan serve --host=127.0.0.1
DOSKEY art=php artisan $*
DOSKEY tinker=php artisan tinker
DOSKEY refresh=php artisan migrate:refresh --seed
DOSKEY fresh=php artisan migrate:fresh --seed
:: 2 Commands in 1 Row
DOSKEY test=cls $T php artisan test
DOSKEY testf=cls $T php artisan test --filter $*

:: PHPUnit
DOSKEY phpunit="vendor/bin/phpunit"
DOSKEY pf="vendor/bin/phpunit" --filter $*

:: Composer Dump Autoload
DOSKEY cda=composer dump-autoload

:: Git
DOSKEY gitcommit = git commit -m $*
DOSKEY gitconfig="C:\Program Files\Sublime Text 3\sublime_text.exe" "C:\Users\DELL\.gitconfig"
DOSKEY git-save = git add .$Tgit stash save --keep-index
DOSKEY wip = git add .$Tgit commit -m "WIP"
DOSKEY gitlog = git log --pretty=oneline







:: Common directories
::DOSKEY lh=cd "C:\Program Files (x86)\Ampps\www\$*"
::DOSKEY localhost=cd "C:\Program Files (x86)\Ampps\www\$*"
::DOSKEY nodes = C:$Tcd "C:\nodes\$*"

:: Common Programs
::DOSKEY ampps="C:\Program Files (x86)\Ampps\Ampps.exe"
::DOSKEY gitcmd="C:\Program Files\Git\bin\sh.exe" --login -i

:: Trying to be Linux?
::DOSKEY ls=dir
::DOSKEY cat=type
::DOSKEY ip=ipconfig
::DOSKEY rm=rmdir /S $*$Tdel $*
::DOSKEY mkdir=mkdir $1$Tcd $1
::DOSKEY touch=copy nul $* > nul
::DOSKEY clear=cls

:: Conemu
::DOSKEY reload=cls$Tcmd cur_console
::DOSKEY new=cmd -new_console:s$*

:: PHP
::DOSKEY php5="C:\Program Files (x86)\Ampps\php-5.6\php.exe" $*
:: Edit PHP.INI file
::DOSKEY phpini="C:\Program Files\Sublime Text 3\sublime_text.exe" "C:\Program Files (x86)\Ampps\php\php.ini"

:: Node.js Commands
::DOSKEY gw=gulp watch
::DOSKEY nrw=npm run watch $*
::DOSKEY nrp=npm run prod $*
::DOSKEY nrt=npm run test $*
::DOSKEY nrtdd=npm run tdd $*
::DOSKEY nr=npm run $*

:: Windows Shutdown set seconds
::DOSKEY st=shutdown /s /t $*
:: Shutdown abort
::DOSKEY sta=shutdown /a