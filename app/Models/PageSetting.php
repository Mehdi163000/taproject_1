<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

//class PageSetting extends Model
class PageSetting extends Authenticatable implements JWTSubject
{
    use HasFactory;

    protected $guarded = ['id'];
    //------------------------------------
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    //----------------------------------
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }
    //----------------------------------
}
