<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\DataType
 *
 * @property int $id
 * @property string $Name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Column[] $columns
 * @property-read int|null $columns_count
 * @method static \Illuminate\Database\Eloquent\Builder|DataType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DataType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|DataType query()
 * @method static \Illuminate\Database\Eloquent\Builder|DataType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|DataType whereName($value)
 * @mixin \Eloquent
 */
class DataType extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public $timestamps = false;

    public function columns()
    {
        return $this->hasMany(Column::class,'id', 'DataType');
    }

}
