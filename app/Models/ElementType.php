<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ElementType
 *
 * @property int $id
 * @property string $Name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ElementWidth[] $elementWidths
 * @property-read int|null $element_widths_count
 * @method static \Illuminate\Database\Eloquent\Builder|ElementType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ElementType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ElementType query()
 * @method static \Illuminate\Database\Eloquent\Builder|ElementType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ElementType whereName($value)
 * @mixin \Eloquent
 */
class ElementType extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public $timestamps = false;

    public function elementWidths()
    {
        return $this->hasMany(ElementWidth::class,'id', 'ElementType');
    }




}
