<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ElementWidth
 *
 * @property int $id
 * @property int $ElementType
 * @property int $ElementID
 * @property string $Name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Column[] $columns
 * @property-read int|null $columns_count
 * @property-read \App\Models\ElementType $elementType
 * @method static \Illuminate\Database\Eloquent\Builder|ElementWidth newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ElementWidth newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ElementWidth query()
 * @method static \Illuminate\Database\Eloquent\Builder|ElementWidth whereElementID($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ElementWidth whereElementType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ElementWidth whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ElementWidth whereName($value)
 * @mixin \Eloquent
 */
class ElementWidth extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public $timestamps = false;

    public function columns()
    {
        return $this->hasMany(Column::class,'id', 'ElementWidth');
    }

    public function elementType()
    {
        return $this->belongsTo(ElementType::class, 'ElementType');
    }
}
