<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Column
 *
 * @property int $id
 * @property int $user_id
 * @property string $Title
 * @property int|null $DataType
 * @property int|null $FormElement
 * @property int|null $ElementWidth
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\DataType|null $columnDataType
 * @property-read \App\Models\ElementWidth|null $columnElementWidth
 * @property-read \App\Models\FormElement|null $columnFormElement
 * @property-read \App\Models\User $user
 * @method static \Database\Factories\ColumnFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Column newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Column newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Column query()
 * @method static \Illuminate\Database\Eloquent\Builder|Column whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Column whereDataType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Column whereElementWidth($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Column whereFormElement($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Column whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Column whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Column whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Column whereUserId($value)
 * @mixin \Eloquent
 */
class Column extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    //------------------------------------

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function columnDataType()
    {
        return $this->belongsTo(DataType::class, 'DataType');
    }

    public function columnFormElement()
    {
        return $this->belongsTo(FormElement::class, 'FormElement');
    }

    public function columnElementWidth()
    {
        return $this->belongsTo(ElementWidth::class, 'ElementWidth');
    }


}
