<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\FormElement
 *
 * @property int $id
 * @property string $Name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Column[] $columns
 * @property-read int|null $columns_count
 * @method static \Illuminate\Database\Eloquent\Builder|FormElement newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FormElement newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FormElement query()
 * @method static \Illuminate\Database\Eloquent\Builder|FormElement whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|FormElement whereName($value)
 * @mixin \Eloquent
 */
class FormElement extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public $timestamps = false;

    public function columns()
    {
        return $this->hasMany(Column::class,'id', 'FormElement');
    }
}
