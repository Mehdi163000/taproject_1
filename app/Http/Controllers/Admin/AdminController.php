<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Column;
use App\Models\DataType;
use App\Models\ElementType;
use App\Models\ElementWidth;
use App\Models\FormElement;
use App\Models\PageSetting;
use App\Models\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //return view('Admin.index');
        $Users = User::paginate( 30);
        $Active = 'Users';
        return view('Admin.Users', compact('Users', 'Active'));

    }

    public function Dashboard()
    {
        //return view('Admin.index');
        $Users = User::paginate( 30);
        $Active = 'Users';
        return view('Admin.Users', compact('Users', 'Active'));

    }

    public function Users()
    {
        $Users = User::paginate( 30);
        $Active = 'Users';
        return view('Admin.Users', compact('Users', 'Active'));

    }

    public function Columns()
    {
        $Columns = Column::paginate(30);
        $Active = 'Columns';
        return view('Admin.Columns', compact('Columns', 'Active'));

    }

    public function DataTypes()
    {
        $DataTypes = DataType::paginate(30);
        $Active = 'DataTypes';
        return view('Admin.DataTypes', compact('DataTypes', 'Active'));

    }

    public function FormElements()
    {
        $FormElements = FormElement::paginate(30);
        $Active = 'FormElements';
        return view('Admin.FormElements', compact('FormElements', 'Active'));

    }

    public function ElementWidths()
    {
        $ElementWidths = ElementWidth::paginate(30);
        $Active = 'ElementWidths';
        return view('Admin.ElementWidths', compact('ElementWidths', 'Active'));

    }

    public function ElementTypes()
    {
        $ElementTypes = ElementType::paginate(30);
        $Active = 'ElementTypes';
        return view('Admin.ElementTypes', compact('ElementTypes', 'Active'));

    }

    public function PageSettings()
    {
        $PageSettings = PageSetting::paginate(30);
        $Active = 'PageSettings';
        return view('Admin.PageSettings', compact('PageSettings', 'Active'));

    }




}
