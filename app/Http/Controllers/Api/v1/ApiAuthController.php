<?php

namespace App\Http\Controllers\Api\v1;
use App\Http\Controllers\Controller;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use JWTAuth;
use App\Models\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;

class ApiAuthController extends Controller
{
    /**
     *
     * @OA\Post(path="/register",
     *      security={{"bearerAuth":{}}},
     *      @OA\Response(response="200",
     *          description="User Register",
     *      ),
     *     @OA\RequestBody(
     *       required=true,
     *       @OA\JsonContent(ref="#/components/schemas/RegisterRequest")
     *   )
     * )
     */

    public function register(RegisterRequest $request)
    {
//        try {

            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password)
            ]);

            // User created, return success response
            return response()->json([
                'success' => true,
                'message' => 'User created successfully',
                'data' => $user
            ], Response::HTTP_OK);

//        }catch (\Exception $e){
//            return response()->json([
//                'success' => false,
//                'message' => $e->getMessage(),
//            ], Response::HTTP_FAILED_DEPENDENCY);
//        }

    }

    public function authenticate(LoginRequest $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json([
                    'success' => false,
                    'message' => 'Username or password is invalid.',
                ], 400);
            }
        } catch (JWTException $e) {
            return $credentials;
//            return response()->json([
//                'success' => false,
//                'message' => 'Could not create token.',
//            ], 500);
        }

        // Token created, return with success response and jwt token
        // Refresh User Token
        //$token = $this->refresh();
        return response()->json([
            'success' => true,
            'token' => $token,
        ]);
    }

    public function logout(Request $request)
    {
        //valid credential
        $validator = Validator::make($request->only('token'), [
            'token' => 'required'
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

        //Request is validated, do logout
        try {
            JWTAuth::invalidate($request->token);

            return response()->json([
                'success' => true,
                'message' => 'User has been logged out'
            ]);
        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, user cannot be logged out'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function get_user(Request $request)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);

        $user = JWTAuth::authenticate($request->token);

        return response()->json(['user' => $user]);
    }

    public function refresh()
    {
//        //$newToken = auth()->refresh();
//
//        // Pass true as the first param to force the token to be blacklisted "forever".
//        // The second parameter will reset the claims for the new token
        //$newToken = auth()->refresh(true, true);
        return $this->respondWithToken(auth()->refresh());

//        $token = $this->respondWithToken(auth()->refresh());
//
//        $user = User::find(auth()->user()->id);
//        $user->api_token = $token;
//        $user->save();  // Update the data
//
//        return $token;

    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            //'expires_in' => auth()->factory()->getTTL() * 60
            'expires_in' => 20 * 60
        ]);
    }





}
