<?php

namespace App\Http\Controllers\Api\v1;
use App\Http\Controllers\Controller;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\Column;
use App\Models\DataType;
use App\Models\ElementType;
use App\Models\ElementWidth;
use App\Models\FormElement;
use App\Models\PageSetting;
use JWTAuth;
use App\Models\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use function PHPUnit\Framework\isNull;

class PageSettingApiController extends Controller
{
    /**
     *
     * @OA\Post(path="/pageSetting/SaveData",
     *      security={{"bearerAuth":{}}},
     *      @OA\Response(response="200",
     *          description="User Register",
     *      ),
     *     @OA\RequestBody(
     *       required=true,
     *    @OA\JsonContent(
     *       @OA\Property(property="PageName", type="string", example="Columns"),
     *   )
     * )
     * )
     */
    public function SaveData(Request $request)
    {
        //----------------------------
        $validation = $this->ValidationEntryData($request);
        if ($validation !== 1) return $validation;
        //----------------------------
        $pageSetting = $this->SaveDataInDatabase($request);

        return response()->json($pageSetting);
        //--------------------------------
    }

    private function SaveDataInDatabase(Request $request)
    {
        try{
            $user_id = auth()->user()->id;
            $PageName = $request->PageName;
            $DataInPage = $request->DataInPage;
            //-----------------------------------
            $pageSettingInDB = PageSetting::where('PageName', $PageName)->first();
            if (isset($pageSettingInDB)){
                $pageSettingInDB->user_id = $user_id;
                //$pageSettingInDB->PageName = $PageName;
                $pageSettingInDB->DataInPage = $DataInPage;

                $pageSettingInDB->save();
                $pageSetting = $pageSettingInDB;
            }else{
                $pageSetting = new PageSetting();
                $pageSetting->user_id = $user_id;
                $pageSetting->PageName = $PageName;
                $pageSetting->DataInPage = $DataInPage;

                $pageSetting->save();
            }
            //----------
            return $pageSetting;


        } catch (\Exception $ex) {
            return response(['Error' => $ex->getMessage(), 'status' => 403], 403);
        }
    }

    private function ValidationEntryData(Request $request)
    {
        try{
            $PageName = $request->PageName;
            $DataInPage = $request->DataInPage;

            if ($PageName == null) return response()->json("نام صفحه را ارسال نمایید");
            if ($DataInPage == null) return response()->json("تعداد فیلد در صفحه را ارسال نمایید");

            return 1;
        } catch (\Exception $ex) {
            return response(['Error' => $ex->getMessage(), 'status' => 403], 403);
        }
    }









}
