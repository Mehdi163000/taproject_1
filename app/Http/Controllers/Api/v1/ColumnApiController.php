<?php

namespace App\Http\Controllers\Api\v1;
use App\Http\Controllers\Controller;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\Column;
use App\Models\DataType;
use App\Models\ElementType;
use App\Models\ElementWidth;
use App\Models\FormElement;
use JWTAuth;
use App\Models\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use function PHPUnit\Framework\isNull;

class ColumnApiController extends Controller
{


    public function index() // Api First Test & Index page for Api
    {
        try {
            //$x = 20 /0; // For Error Test

            return response(['data' => ['data1' => 'Ok'], 'status' => 200], 200);


        } catch (\Exception $ex) {
            return response(['Error' => $ex->getMessage(), 'status' => 403], 403);
        }
    }


    /**
     *
     * @OA\Post(path="/column/SaveData",
     *      security={{"bearerAuth":{}}},
     *      @OA\Response(response="200",
     *          description="User Register",
     *      ),
     *     @OA\RequestBody(
     *       required=true,
     *    @OA\JsonContent(
     *       @OA\Property(property="Title", type="string", example="ColumnName"),
     *   )
     * )
     * )
     */
    public function SaveData(Request $request)
    {
        //----------------------------
        $validation = $this->ValidationEntryData($request);
        if ($validation !== 1) return $validation;
        //----------------------------
        $column = $this->SaveColumnDataInDatabase($request);

        return response()->json($column);
        //--------------------------------
        //return 'اطلاعات با موفقیت در پایگاه داده ذخیره شد.' . 'شماره درخواست : ' . $column->id;
        //--------------------------------
    }

    private function SaveColumnDataInDatabase(Request $request)
    {
        try{
            $user_id = auth()->user()->id;
            $Title = $request->Title;
            $DataType = $request->DataType;
            $FormElement = $request->FormElement;
            $ElementWidth = $request->Width;
            $ElementFontSize = $request->FontSize;
            $ElementFontWeight = $request->FontWeight;
            //-----------------------------------
            $column = new Column();
            $column->user_id = $user_id;
            $column->Title = $Title;
            //----------
            if ($DataType != null) {
                $type = DataType::where('name', $DataType)->first();
                if (!isset($type)){
                    $type = new DataType();
                    $type->name = $DataType;
                    $type->save();
                }

                $DataType = $type->id;
            }

            $column->DataType = $DataType;
            //----------
            if ($FormElement != null) {
                $fElement = FormElement::where('name', $FormElement)->first();
                if (!isset($fElement)){
                    $fElement = new FormElement();
                    $fElement->name = $FormElement;
                    $fElement->save();
                }

                $FormElement = $fElement->id;
            }

            $column->FormElement = $FormElement;
            //----------
            $column->save();
            //--------------
            if ($ElementWidth != null or $ElementFontSize != null or $ElementFontWeight != null) {

                $eWidth = new ElementWidth();

                $eType = ElementType::where('name', 'column')->first();
                $eWidth->ElementType = $eType->id; // Column

                $eWidth->ElementID = $column->id;
                $eWidth->Width = $ElementWidth;
                $eWidth->FontSize = $ElementFontSize;
                $eWidth->FontWeight = $ElementFontWeight;

                $eWidth->save();

                $ElementWidth = $eWidth->id;
            }
            //--------------
            $column = Column::find($column->id);
            $column->ElementWidth = $ElementWidth;
            $column->save();
            //----------
            return $column;


        } catch (\Exception $ex) {
            return response(['Error' => $ex->getMessage(), 'status' => 403], 403);
        }
    }

    private function ValidationEntryData(Request $request)
    {
        try{
            //$user_id = auth()->user()->id;
            $Title = $request->Title;
            $DataType = $request->DataType;
            $FormElement = $request->FormElement;
            $ElementWidth = $request->Width;
            $ElementFontSize = $request->FontSize;
            $ElementFontWeight = $request->FontWeight;

            if ($Title == null) return response()->json("عنوان ستون را ارسال نمایید");
            if ($DataType == null) return response()->json("نوع داده را ارسال نمایید");

            //return response()->json($DataType);

            return 1;
        } catch (\Exception $ex) {
            return response(['Error' => $ex->getMessage(), 'status' => 403], 403);
        }
    }









}
