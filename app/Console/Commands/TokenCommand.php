<?php

namespace App\Console\Commands;

use App\Http\Controllers\Api\v1\ApiAuthController;
use http\Env\Request;
use Illuminate\Console\Command;
use App\Models\User;
use Illuminate\Support\Facades\Crypt;
use Symfony\Component\Console\Output\ConsoleOutput;

class TokenCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    //protected $signature = 'command:name';
    protected $signature = 'token:generate {email}{password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';



    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
//        $id = $this->argument('id');
//
//        $user = User::find($id);
//
//        \Auth::setUser($user);
//
//        $console = new ConsoleOutput();
//
//        //$console->writeln($user->createToken('admin')->accessToken);
//
//        //'token' => hash('sha256', $plainTextToken = Str::random(40)),
//
//         //$console->writeln($user->api_token);
//
//         $controller = new ApiAuthController();
//        $request = new \App\Http\Requests\LoginRequest(['email' => $user->email, 'password' => Crypt::decrypt($user->password)]);
//        $console->writeln( $controller->login($request));
        //---------------------------------
        $email = $this->argument('email');
        $password = $this->argument('password');
        $console = new ConsoleOutput();

        $controller = new ApiAuthController();
        $request = new \App\Http\Requests\LoginRequest(['email' => $email, 'password' => $password]);
        $console->writeln( $controller->authenticate($request));




        //return Command::SUCCESS;
    }
}
